import sys
import json
import glob
import os
import re
from os.path import dirname, abspath
from pprint import pprint as pp

DEFAULT_STARS = set([ "stars0", "stars1", "stars2", "stars3" ])

class Catalog(object):

    def __init__(self, h):
        self.h = h

    @property
    def catalogs(self):
        arr = [ x for x in self.h['catalogs'] if x['id'] not in DEFAULT_STARS ]
        return sorted(arr, key = lambda x: x['id'])

    @property
    def idents(self):
        return [ x['id'] for x in self.catalogs ]

    @property
    def files_info(self):
        idents = self.idents
        return [
            {
                'name' : x['fileName'],
                'checksum' : x['checksum'],
                'id' : x['id']
            } for x in self.catalogs ]

    def file_info(self, ident):
        arr = [ x for x in self.files_info if x['id'] == ident ]
        assert len(arr) == 1
        return arr[0]

    def __repr__(self):
        return 'Catalog v{}'.format(self.h["version"])

def load_file(name):
    with file(name, "rb") as f:
        return f.read()

def save_file(name, content):
    with file(name, "wb") as f:
        f.write(content)

def versions():
    info = {}
    versions = glob.glob("versions/*.json")
    for v in versions:
        d = json.loads(load_file(v))
        info[d['version']] = Catalog(d)
    return info

def generate(version):
    print 'Generating files for version {}'.format(version)
    vs = versions()
    current = vs[version]

    FIRST = 2  # FIRST version considered

    # ensure consistency
    for v in vs.values():
        assert v.idents == vs[4].idents

    previous = {}

    for info in current.files_info:
        print ' * Considering {}'.format(info["id"])
        it = version
        while it >= FIRST:
            v = vs[it]
            if v.file_info(info['id']) != info:
                print '   Version {} differs.'.format(it)
                break
            it -= 1
        previous[info['id']] = it

    generate_debian_files(current, version)

def generate_debian_files(v, version):
    postinst_template = load_file("templates/postinst.in")
    prerm_template = load_file("templates/prerm.in")
    control_template = load_file("templates/control.in")
    real_version = "1.{}.0".format(version) # TODO

    packages = []

    for cat in v.catalogs:
        md5 = cat["checksum"]
        size = cat["sizeMb"]
        url = cat["url"]
        name = cat["fileName"]
        magn = cat["magRange"]
        count = cat["count"]
        ident = cat["id"]

        postinst = postinst_template.\
            replace("#STARS_FILE#", name).\
            replace("#STARS_CHECKSUM#", md5).\
            replace("#STARS_URL#", url)

        prerm = prerm_template.\
            replace("#STARS_FILE#", name).\
            replace("#STARS_VERSION#", real_version)

        package = "stellarium-{}".format(ident)

        save_file("{}.postinst".format(package), postinst)
        save_file("{}.prerm".format(package), prerm)

        no = int(re.findall(r"(\d+)", ident)[0])

        packages.append("Package: {}".format(package))
        packages.append("Architecture: all")
        packages.append("Depends: ${misc:Depends}, stellarium (>= 0.12.2)")  # TODO
        packages.append("Description: Stellarium star catalogue no. {}".format(no))
        packages.append(" This package contains additional star catalogue with")
        packages.append(" about {} millions of stars with magnitudo in range {}.".format(count, magn))
        packages.append(" Installation of this package requires a working")
        packages.append(" Internet connection and will download {} MBs of data.".format(size))
        if no >= 6:
            packages.append(" WARNING: Note that this package is quite big and Stellarium")
            packages.append(" may refuse to work if your machine is not powerful enough.")
            packages.append(" If your Stellarium refuses to work, uninstall this package.")
        packages.append("")

        # lintian overrides
        if ident != 'stars4':
            save_file("{}.lintian-overrides".format(package), "{}: empty-binary-package".format(package))

    control = control_template.replace("#PACKAGES#", "\n".join(packages))
    save_file("control", control)

def main():
    if len(sys.argv) == 1:
        vs = versions().keys()
        print 'There are {} versions of star packs:'.format(len(vs))
        for v in vs:
            print "  Version {}".format(v)
        print "Use {} <version> to generate file for a particular version".format(sys.argv[0])
    elif len(sys.argv) == 2:
        generate(int(sys.argv[1]))
    else:
        print "Usage: {} [version]".format(sys.argv[0])
        sys.exit(1)

if __name__ == '__main__':
    os.chdir(abspath(dirname(__file__)))
    main()
